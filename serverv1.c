#include<stdio.h>
#include<sys/types.h>//socket
#include<sys/socket.h>//socket
#include<string.h>//memset
#include<stdlib.h>//sizeof
#include<netinet/in.h>//INADDR_ANY
#include <dirent.h> 

//i plus
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdarg.h>
#include <signal.h>

#define PRIME 4
#define PORT 6000
#define LISTENQ 20
#define BUFFSIZE 4096
#define FILE_NAME_MAX_SIZE 512

char downfile(char msg[BUFFSIZE],int newsockfd,char newpath[30],char *buf){
	recv(newsockfd,msg,BUFFSIZE,0);
	
	char filename[FILE_NAME_MAX_SIZE];
	char test[BUFFSIZE];
	
	strcpy(filename,msg);
	//send file imformation
	//int count;
	bzero(msg,BUFFSIZE);
	
	strcat(newpath,filename);
	
	//read file 
	FILE *fd=fopen(filename,"rb");
	if(fd==NULL){
		
		strcpy(test,"t");
		send(newsockfd,test,BUFFSIZE,0);
		
		printf("File :%s not found!\n",filename);
		
	}else{
		
		bzero(msg,BUFFSIZE);
		int file_block_length=0;
		while((file_block_length=fread(msg,sizeof(char),BUFFSIZE,fd))>0){
			printf("file_block_length:%d\n",file_block_length);
			if(send(newsockfd,msg,file_block_length,0)<0){
				perror("Send");
				exit(1);
			}
			bzero(msg,BUFFSIZE);	
		}
		fclose(fd);
		printf("Transfer file finished !\n");
	}
}

int main(int argc, char **argv[])
{
  //signal//
 static struct sigaction act; 
 
 void catchin(int);
 
 act.sa_handler = catchin; 
 sigfillset (&(act.sa_mask));

 int sockfd;//to create socket
 int newsockfd;//to accept connection


 struct sockaddr_in serverAddress;//server receive on this address
 struct sockaddr_in clientAddress;//server sends to client on this address

 int n;
 char msg[BUFFSIZE]; 
 char check[BUFFSIZE]; 
 char menu[BUFFSIZE];
 char del[BUFFSIZE];
 int clientAddressLength;
 int pid;
 
  //message queue part
 int msqid;
 key_t key;
 
 if ((key = ftok("server.c", 'B')) == -1)
 {
	perror("ftok() error\n");
 	exit(1);
 }
 if ((msqid = msgget(key, 0644 | IPC_CREAT)) == -1)
 {
 	perror("mssget fail");
	exit(1);
 }
 
 //create socket
 sockfd=socket(AF_INET,SOCK_STREAM,0);
 //initialize the socket addresses
 memset(&serverAddress,0,sizeof(serverAddress));
 serverAddress.sin_family=AF_INET;
 serverAddress.sin_addr.s_addr=htonl(INADDR_ANY);
 serverAddress.sin_port=htons(PORT);

 //bind the socket with the server address and port
 if (bind(sockfd,(struct sockaddr *)&serverAddress, sizeof(serverAddress)) <0 ){
	perror("bind");
	exit(1);
 }

 //listen for connection from client
 if(listen(sockfd,LISTENQ)<0)
	{
		perror("listen");
		exit(1);
	}

	char *buf; 
	buf=(char *)malloc(10*sizeof(char)); 
	buf=getlogin(); 
	
	char newpath[30]; 
	strcpy(newpath, "/home/"); 
	strcat(newpath, buf); 
	strcat(newpath, "/Desktop/serv_file/");

	struct stat st; 
	if(stat(newpath, &st) == -1){ 
	mkdir(newpath, 0700); }	
	
 while(1)
 {
  //parent process waiting to accept a new connection
  printf("\n*****server waiting for new client connection:*****\n");
  clientAddressLength=sizeof(clientAddress);
  newsockfd=accept(sockfd,(struct sockaddr*)&clientAddress,&clientAddressLength);
  if (newsockfd<0){
	perror("connect");
	exit(1);
  }
  printf("connected to client: %d\n",inet_ntoa(clientAddress.sin_addr));

  //child process is created for serving each new clients
  pid=fork();
  if(pid==0){//child process rec and send
   //rceive from client
   while(1){
    
    strcpy(menu,"1 Upload file to server\n2 List file\n3 Delete file\n4 Download File\n5 EXIT\n\nPlease Choose:\n");
    send(newsockfd,menu,sizeof(menu),0);

    n=recv(newsockfd,msg,BUFFSIZE,0);
    if(n==0){
     close(newsockfd);
     break;
    }
    char filename[FILE_NAME_MAX_SIZE];
    char test[BUFFSIZE];
    int length=0;

    send(newsockfd,msg,BUFFSIZE,0);
    
    if(msg[0] == '1'){
        printf("Receive and set:%s\n",msg);
	//recv(newsockfd,msg,BUFFSIZE,0);
	strcpy(filename,msg);
		
	bzero(test,BUFFSIZE);
	length = recv(newsockfd,test,BUFFSIZE,0);

	if(test[0] != 't'){
		strcat(newpath,filename);	
		FILE *fd=fopen(newpath,"wb+");
		fscanf(fd, "%s", msg);
	
	    if(NULL==fd){
		  perror("open");
	    }
	    //bzero(msg,BUFFSIZE);

	    while(length){
		if(length<=0){
			//perror("recv");
			printf("WTP!!");
			break;
		}
		int writelen=fwrite(msg,sizeof(char),length,fd);
		if(writelen<=length){
			//perror("write");
			printf("\nSuccessful Received file from client!\n\n");
			break;
		}
		bzero(msg,BUFFSIZE);
	  }
	  fclose(fd);
	}else{printf("File Not FOUND!\n");}
    }
    else if(msg[0] == '2'){
        printf("Receive and set:%s\n",msg);
    }
    else if(msg[0] == '3'){
        printf("Receive and set:%s\n",msg);
	recv(newsockfd,msg,BUFFSIZE,0);
		strcpy(filename,msg);
		
		strcat(newpath,filename);
		
	   int status;
	   status = remove(newpath);
 
   	   if( status == 0 ){
          printf("%s file deleted successfully.\n",filename);
   	   }else{
      	  	printf("Unable to delete the file\n");
   	   }
    }
	else if(msg[0] == '4'){
        printf("Receive and set:%s\n",msg);
	
	downfile(msg,newsockfd,newpath,buf);
    }
    else if(msg[0] == '5'){
        printf("Receive and set:%s\n",msg);
	    printf("......Client Exit......\n");
    }

   }//close interior while
  exit(0);
  }
  else{
   close(newsockfd);//sock is closed BY PARENT
  }
 }//close exterior while

 return 0;
}

void catchin (int signo)
{
	printf("\nCATCHINT: signo =%d\n", signo);
}
