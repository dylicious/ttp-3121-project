# README #

### What is this repository for? ###

* Client Server in TCP/IP Model
* Uses TCP to communicate
* Allow multiple client to make transactions concurrently
* Make use of process controls such as fork() and signals
* Establish Pipe/FIFO and Message Queue to send files
* Send files!

### How do I get set up? ###

* Software : Terminal
* Operating System : Linux / Unix OS
* Run a terminal and execute server.c

```
#!command line

$ gcc serverv1.c -o serverv1
$ ./serverv1
```
* Run and open another terminal to execute client.c

```
#!command line

$ gcc clientv1.c -o clientv1
$ ./clientv1
```

* Send your files!

### TEAM ###

* WENDY GOH WEN YIT (1122701837)
* CHUA HUI KHEN (1122701414)