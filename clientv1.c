/************Client Code***********/
//CLIENT ITERATIVE

#include<stdio.h>
#include<sys/types.h>//socket
#include<sys/socket.h>//socket
#include<string.h>//memset
#include<stdlib.h>//sizeof
#include<netinet/in.h>//INADDR_ANY
#include <dirent.h> 

//i plus
#include<errno.h>
#include <unistd.h>
#include <sys/stat.h>
#include<sys/ipc.h>
#include<sys/msg.h>

#define PORT 6000
#define SERVER_IP "127.0.0.1"
#define LISTENQ 20
#define BUFFSIZE 4096
#define FILE_NAME_MAX_SIZE 512

char sendfile(char msg1[BUFFSIZE],int sockfd,char newpath[30]){
	//Input the file name
	char filename[FILE_NAME_MAX_SIZE];
	char test[BUFFSIZE];

	bzero(filename,FILE_NAME_MAX_SIZE);
	printf("Please input the file name you wanna to send:");
	scanf("%s",filename);
	getchar();
	
	//send file imformation
	int count;
	
	bzero(msg1,BUFFSIZE);
	
	strcat(newpath, filename);
	
	//read file 
	FILE *fd=fopen(newpath,"rb");
	if(fd==NULL){
		strcpy(test,"t");
		send(sockfd,test,BUFFSIZE,0);
		printf("File :%s not found!\n",filename);
		exit(1);
	}else{
		count=send(sockfd,filename,BUFFSIZE,0);
	if(count<0){
		perror("Send file imformation");
		exit(1);
	}
		bzero(msg1,BUFFSIZE);
		int file_block_length=0;
		while((file_block_length=fread(msg1,sizeof(char),BUFFSIZE,fd))>0){
			printf("file_block_length:%d\n",file_block_length);
			if(send(sockfd,msg1,file_block_length,0)<0){
				perror("Send");
				exit(1);
			}
			bzero(msg1,BUFFSIZE);	
		}
		fclose(fd);
		printf("Transfer file finished !\n");
	}
}

char downfile(char msg1[BUFFSIZE],int sockfd,char newpath[30]){
	printf("Please input the file name you wanna to download:");
	scanf("%s",msg1);
	
	//send file imformation
	send(sockfd,msg1,BUFFSIZE,0);

	char filename[FILE_NAME_MAX_SIZE];
	char test[BUFFSIZE];

	int length=0;
	   strcpy(filename,msg1);
	   bzero(test,BUFFSIZE);
	   recv(sockfd,test,BUFFSIZE,0);
	   length = recv(sockfd,test,BUFFSIZE,0);
	   strcpy(msg1,test);
	if(test[0] != 't'){
		strcat(newpath, filename);
		
		FILE *fdd=fopen(newpath,"wb+");
		if(NULL==fdd){
		perror("open");
		}
		
		while(length){
		if(length<=0){
			perror("recv");
			break;
		}
		int writelen=fwrite(msg1,sizeof(char),length,fdd);
		if(writelen<=length){
			printf("\nSuccessful Received file from SERVER!\n\n");
			break;
		}
		bzero(msg1,BUFFSIZE);
		fclose(fdd);
		}
	    }else{printf("File Not FOUND!\n");exit(1);}
}

char listfile(char newpath[30]){
  
  DIR           *d;
  struct dirent *dir;
  d = opendir(newpath);
  int countfile=0;
  if (d){
    while ((dir = readdir(d)) != NULL){
      printf("%s\n",dir->d_name);
    }
    printf("\n");
    closedir(d);
  }
}

int main(int argc, char **argv[])
{
 int sockfd;//to create socket

 struct sockaddr_in serverAddress;//client will connect on this

 int n;
 char msg1[BUFFSIZE];
 char msg2[BUFFSIZE];
 char menu[BUFFSIZE];

    struct sockaddr_in clientaddr;
	bzero(&clientaddr,sizeof(clientaddr));	

	clientaddr.sin_family=AF_INET;
	clientaddr.sin_addr.s_addr=htons(INADDR_ANY);
	clientaddr.sin_port=htons(0);
 
 //for message queue purpose
 int msqid;
 key_t key;

 if ((key == ftok("server.c", 'B')) == -1)
 {
	perror("ftok error");
	exit(1);
 }

 if ((msqid = msgget(key, 0644)) == -1)
 {
	perror("msgget");
 	exit(1);
 }
 
 //create socket
 sockfd=socket(AF_INET,SOCK_STREAM,0);
 
 if(sockfd<0)	
	{
		perror("socket");
		exit(1);
	}

	if(bind(sockfd,(struct sockaddr*)&clientaddr,sizeof(clientaddr))<0)
	{
		perror("bind");
		exit(1);
	}
 
 //initialize the socket addresses
 memset(&serverAddress,0,sizeof(serverAddress));
 serverAddress.sin_family=AF_INET;
 //serverAddress.sin_addr.s_addr=inet_addr(SERVER_IP);
serverAddress.sin_addr.s_addr=htons(INADDR_ANY);
 serverAddress.sin_port=htons(PORT);

 //client  connect to server on port
 connect(sockfd,(struct sockaddr *)&serverAddress,sizeof(serverAddress));

char *buf; 
	buf=(char *)malloc(10*sizeof(char)); 
	buf=getlogin(); 
	
	char newpath[30]; 
	strcpy(newpath, "/home/"); 
	strcat(newpath, buf); 
	strcat(newpath, "/Desktop/client_file/");

	struct stat st; 
	if(stat(newpath, &st) == -1){ 
	mkdir(newpath, 0700); }	

 //send to sever and receive from server
 while(1)
 {  
    //bzero(msg2, sizeof(msg2));
    recv(sockfd,menu,BUFFSIZE,0);
    printf("Receive message from  server::\n%s",menu);

    fgets(msg1,BUFFSIZE,stdin);
    
    if(strlen(msg1) == 2){
      send(sockfd,msg1,sizeof(msg1),0);
      //bzero(msg2, sizeof(msg2));
      //recv(sockfd,msg2,sizeof(msg2),0);
      printf("Receive message from  server::\n%s",msg1);
	  if(msg1[0] == '1'){
	     sendfile(msg1,sockfd,newpath);
	
      }
	  else if(msg1[0] == '2'){
	     printf("The list of FILE::");
	     listfile(newpath);
      }
	  else if(msg1[0] == '3'){
	     char delfile[FILE_NAME_MAX_SIZE];
	     
	     printf("Please Enter the filename that u wanna DELETE: ");
	     scanf("%s",delfile);

	     FILE *fd=fopen(delfile,"rb");
	     if(fd==NULL){
		printf("File :%s not found!\n",delfile);
		exit(1);
	     }else{
		send(sockfd,delfile,BUFFSIZE,0);
	     }
	    fclose(fd);
      }
	  else if(msg1[0] == '4'){
	    
	
	

	downfile(msg1,sockfd,newpath);
	exit(1);
	  
	  }
	  else if(msg1[0] == '5'){
	     strcpy(msg1,"5");
         send(sockfd,msg1,sizeof(msg1),0);
		 exit(0);
	  }else{
	     strcpy(msg1,"5");
         send(sockfd,msg1,sizeof(msg1),0);
         exit(1);
	  }
    }else{
      strcpy(msg1,"5");
      send(sockfd,msg1,sizeof(msg1),0);
      exit(1);
    }

 }

 return 0;
}
